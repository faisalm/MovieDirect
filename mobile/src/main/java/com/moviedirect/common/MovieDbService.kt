package com.moviedirect.common

import androidx.lifecycle.LiveData
import com.moviedirect.detail.model.Movie
import com.moviedirect.list.model.MoviesList
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * An interface consists of APIs
 */
interface MovieDbService {

    /**
     * API to fetch the 'Now Playing' list from The Movie Database (TMDb) API. The sample API call would be
     * https://api.themoviedb.org/3/movie/now_playing?api_key={@valiteral API_KEY}.
     *
     * @param apiKey for user authentication.
     * @return list of Movies in [MoviesList] format.
     */
    @GET("now_playing")
    fun fetchLatestMovies(@Query("api_key") apiKey: String): Call<MoviesList>

    /**
     * API to fetch the 'Now Playing' list from The Movie Database (TMDb) API. The sample API call would be
     * https://api.themoviedb.org/3/movie/now_playing?api_key={@valiteral API_KEY}&page={1}.
     *
     * @param apiKey for user authentication.
     * @param page   to be fetched.
     * @return list of Movies in [MoviesList] format.
     */
    @GET("now_playing")
    fun fetchLatestMoviesPaged(@Query("api_key") apiKey: String, @Query("page") page: Int): Call<MoviesList>

    /**
     * API to fetch details for each [Movie]. The sample API call would be
     * https://api.themoviedb.org/3/movie/{@valiteral MOVIE_ID}?api_key={@valiteral API_KEY}
     *
     * @param movieId to fetch the details of a specific [Movie].
     * @param apiKey  for user authentication.
     * @return the complete detail about a movie in [Movie] format.
     */
    @GET("{movie_id}")
    fun fetchMovieDetails(@Path("movie_id") movieId: Int, @Query("api_key") apiKey: String): Call<LiveData<Movie>>

    @GET("{movie_id}")
    fun getMovieDetailsAsync(@Path("movie_id") movieId: Int, @Query("api_key") apiKey: String):
            Deferred<Response<LiveData<Movie>>>
}
