package com.moviedirect.common

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.moviedirect.detail.MovieDetailViewModel
import com.moviedirect.list.MovieListViewModel

@Suppress("UNCHECKED_CAST")
class ViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MovieListViewModel::class.java)) {
            return MovieListViewModel() as T
        } else if (modelClass.isAssignableFrom(MovieDetailViewModel::class.java)) {
            return MovieDetailViewModel() as T
        }

        throw IllegalArgumentException("Unknown class name")
    }
}