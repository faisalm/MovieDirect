package com.moviedirect.common

import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Factory class for [MovieDbService].
 */
object RetrofitFactory {

    fun create(): MovieDbService {

        // create an RxJava Adapter, network calls made asynchronous
        val rxAdapter = RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io())

        // Build retrofit object
        val retrofit = Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(rxAdapter)
                .build()

        return retrofit.create(MovieDbService::class.java)
    }
}
