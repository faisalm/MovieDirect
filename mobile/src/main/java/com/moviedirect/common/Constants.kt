package com.moviedirect.common

/**
 * App-level Util class.
 */
object Constants {

    /**
     * Base Url to fetch the data from The Movie Database (TMDb) API.
     */
    const val BASE_URL = "https://api.themoviedb.org/3/movie/"

    /**
     * The API key for The Movie Database (TMDb).
     */
    const val API_KEY = "8d2244e4902a0d867813df309e90a710"

    /**
     * Bundle param to be passed while fragment transition.
     */
    const val MOVIE_ID = "movie_id"

    /**
     * Base Utl to fetch the Movie image data.
     */
    const val BASE_IMAGE_URL = "http://image.tmdb.org/t/p/w300/"
}
