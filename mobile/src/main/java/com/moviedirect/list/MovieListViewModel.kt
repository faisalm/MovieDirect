package com.moviedirect.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.moviedirect.list.model.MovieItem

class MovieListViewModel : ViewModel() {

    private val PAGE_SIZE = 10

    private var movies: LiveData<PagedList<MovieItem>>

    init {
        val dataSourceFactory = MovieDataSourceFactory()

        val pagedListConfig = PagedList.Config.Builder()
                .setInitialLoadSizeHint(PAGE_SIZE)
                .setPageSize(PAGE_SIZE)
                .setEnablePlaceholders(true)
                .build()

        movies = LivePagedListBuilder(dataSourceFactory, pagedListConfig)
                // .setBoundaryCallback() TODO
                .build()
    }

    fun getMovies(): LiveData<PagedList<MovieItem>> {
        return movies
    }
}
