package com.moviedirect.list

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.moviedirect.list.model.MovieItem

/**
 *
 */
class MovieDataSourceFactory : DataSource.Factory<Int, MovieItem>() {

    private val mutableLiveData = MutableLiveData<MovieDataSource>()

    override fun create(): DataSource<Int, MovieItem> {
        val dataSource = MovieDataSource()
        mutableLiveData.postValue(dataSource)
        return dataSource
    }
}
