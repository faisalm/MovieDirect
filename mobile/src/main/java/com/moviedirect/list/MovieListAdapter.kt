package com.moviedirect.list

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.moviedirect.R
import com.moviedirect.common.Constants
import com.moviedirect.list.model.MovieItem
import com.squareup.picasso.Picasso

internal class MovieListAdapter internal constructor() : PagedListAdapter<MovieItem, MovieListHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieListHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.movie_list_row, parent, false)
        return MovieListHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MovieListHolder, position: Int) {

        getItem(position)?.let { movieItem ->

            // Movie image
            val posterPath = Constants.BASE_IMAGE_URL + movieItem.poster_path

            Picasso.get().load(posterPath)
                    .fit().centerCrop()
                    .placeholder(R.drawable.ic_movie_place_holder)
                    .error(R.drawable.ic_no_movie_place_holder)
                    .into(holder.imgBanner)

            holder.tvTitle.text = movieItem.title
            holder.ratingBar.rating = movieItem.vote_average
            holder.tvVoting.text = "(" + movieItem.vote_count + ")"

            holder.imgBanner.setOnClickListener { view ->
                val bundle = Bundle()
                bundle.putInt(Constants.MOVIE_ID, movieItem.id)
                Navigation.findNavController(view).navigate(R.id.movieDetailFragment, bundle)
            }
        }
        
    }

    companion object {

        private val diffCallback = object : DiffUtil.ItemCallback<MovieItem>() {

            override fun areItemsTheSame(oldItem: MovieItem, newItem: MovieItem): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: MovieItem, newItem: MovieItem): Boolean {
                return oldItem == newItem
            }
        }
    }
}
