package com.moviedirect.list.model

/**
 * Model class represents the list contains [MovieItem].
 */
data class MoviesList(
        val results: List<MovieItem>? = null,
        val page: Int = 0,
        val total_results: Int = 0,
        val dates: DateRange? = null,
        val total_pages: Int = 0
)