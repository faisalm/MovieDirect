package com.moviedirect.list.model

/**
 * Modal class represents release date of the [MovieItem] are released.
 * The range is specific to each resulting page and w.r.t to [MovieItem] in the [MoviesList]
 */
data class DateRange(
        val maximum: String? = null,
        val minimum: String? = null
)