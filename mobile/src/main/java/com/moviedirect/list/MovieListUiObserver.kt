package com.moviedirect.list

import android.view.View
import android.widget.TextView

import androidx.recyclerview.widget.RecyclerView

class MovieListUiObserver(private val recyclerView: RecyclerView, private val emptyView: TextView?) : RecyclerView.AdapterDataObserver() {

    private val TAG = javaClass.simpleName

    init {
        checkIfEmpty()
    }

    private fun checkIfEmpty() {
        if (emptyView != null && recyclerView.adapter != null) {
            val emptyViewVisible = recyclerView.adapter!!.itemCount == 0
            emptyView.visibility = if (emptyViewVisible) View.VISIBLE else View.GONE
            recyclerView.visibility = if (emptyViewVisible) View.GONE else View.VISIBLE
        }
    }
}
