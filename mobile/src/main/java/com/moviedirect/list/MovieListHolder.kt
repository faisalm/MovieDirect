package com.moviedirect.list

import android.view.View
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.moviedirect.R

internal class MovieListHolder(view: View) : RecyclerView.ViewHolder(view) {
    var imgBanner: ImageView = view.findViewById(R.id.imgMovieBanner)
    var tvTitle: TextView = view.findViewById(R.id.tvTitle)
    var tvVoting: TextView = view.findViewById(R.id.tvVoteCount)
    var ratingBar: RatingBar = view.findViewById(R.id.rbRating)
}

