package com.moviedirect.list

import androidx.paging.PageKeyedDataSource
import com.moviedirect.common.Constants
import com.moviedirect.common.MovieDbService
import com.moviedirect.common.RetrofitFactory
import com.moviedirect.list.model.MovieItem
import com.moviedirect.list.model.MoviesList
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 *
 */
class MovieDataSource internal constructor() : PageKeyedDataSource<Int, MovieItem>() {

    private val movieDbService: MovieDbService = RetrofitFactory.create()

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, MovieItem>) {

        val moviesListCall = movieDbService.fetchLatestMoviesPaged(Constants.API_KEY, 1)
        moviesListCall.enqueue(object : Callback<MoviesList> {
            override fun onResponse(call: Call<MoviesList>, response: Response<MoviesList>) {

                if (response.isSuccessful) {
                    val moviesLists = response.body()?.results
                    callback.onResult(moviesLists!!, 1, 2)
                }
            }

            override fun onFailure(call: Call<MoviesList>, t: Throwable) {}
        })
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, MovieItem>) {}

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, MovieItem>) {

        val moviesListCall = movieDbService.fetchLatestMoviesPaged(Constants.API_KEY, params.key)

        moviesListCall.enqueue(object : Callback<MoviesList> {
            override fun onResponse(call: Call<MoviesList>, response: Response<MoviesList>) {

                if (response.isSuccessful) {
                    val moviesLists = response.body()?.results
                    callback.onResult(moviesLists!!, params.key + 1)
                }
            }

            override fun onFailure(call: Call<MoviesList>, t: Throwable) {}
        })
    }
}
