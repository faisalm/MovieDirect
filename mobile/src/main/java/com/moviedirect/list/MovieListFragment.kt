package com.moviedirect.list

import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.analytics.FirebaseAnalytics
import com.moviedirect.R
import com.moviedirect.common.ViewModelFactory
import com.moviedirect.list.model.MovieItem
import kotlinx.android.synthetic.main.fragment_movie_list.*

class MovieListFragment : Fragment()/*, Observer<PagedList<MovieItem>> */ {

    private val TAG = "MovieListFragment"

    private lateinit var mRecyclerAdapter: MovieListAdapter
    private lateinit var viewModel: MovieListViewModel
    private lateinit var viewModelFactory: ViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Log.d(TAG, "onCreate() called with: savedInstanceState = [$savedInstanceState]")

        // Update screen to Firebase - Manually track screens
        FirebaseAnalytics.getInstance(context!!).setCurrentScreen(activity!!, TAG, "movie_list")

        // View Model
        viewModelFactory = ViewModelFactory()
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MovieListViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_movie_list, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mRecyclerAdapter = MovieListAdapter()

        rvMovieList.apply {
            // Dedicated layouts for Screen Orientation
            if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
                layoutManager = LinearLayoutManager(context)
            } else {
                layoutManager = GridLayoutManager(context, 2)
            }

            adapter = mRecyclerAdapter
        }

        // Empty view
        /*TextView emptyListView = view.findViewById(R.id.empty_recipelist_view);h
        MovieListUiObserver dataChangeObserver = new MovieListUiObserver(mRecyclerView, emptyListView);
        mRecyclerAdapter.registerAdapterDataObserver(dataChangeObserver);*/
    }

    override fun onResume() {
        super.onResume()

        Log.d(TAG, "onResume()")

        // Listen to data change
        viewModel.getMovies().observe(viewLifecycleOwner, myObserver)
    }

    /*override fun onChanged(movieItems: PagedList<MovieItem>) {
        Log.d(TAG, "MovieItems: ${movieItems.size}")
        if (movieItems.isNotEmpty()) {
            mRecyclerAdapter.submitList(movieItems)
        } else {
            mRecyclerAdapter.submitList(null)
        }
    }*/

    private val myObserver: Observer<PagedList<MovieItem>> = Observer { movieItems ->
        Log.d(TAG, "MovieItems: ${movieItems.size}")
        if (movieItems.isNotEmpty()) {
            mRecyclerAdapter.submitList(movieItems)
        } else {
            mRecyclerAdapter.submitList(null)
        }
    }

    override fun onPause() {
        viewModel.getMovies().removeObserver(myObserver)
        Log.d(TAG, "onPause()")

        super.onPause()
    }
}
