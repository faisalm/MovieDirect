package com.moviedirect.detail

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.moviedirect.common.Constants
import com.moviedirect.common.MovieDbService
import com.moviedirect.common.RetrofitFactory
import com.moviedirect.detail.model.Movie
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieDetailViewModel : ViewModel() {

    private val TAG: String = "MovieDetailViewModel"

    private var movieDetails: MutableLiveData<Movie> = MutableLiveData()
//    private var movieId: Int = 0

//    init {
//        Log.d(TAG, "MovieDetailViewModel, init")
//        fetchMovieInDetail(movieId)
//    }

    fun setMovieId(id: Int) {
//        movieId = id
        Log.d(TAG, "setMovieId, movieId: $id")

        fetchMovieInDetail(id)
    }

    fun getMovieDetail(/*movieId: Int*/): MutableLiveData<Movie>? {
//        fetchMovieInDetail(movieId)
        return movieDetails
    }

    /**
     *
     */
    @SuppressLint("CheckResult")
    private fun fetchMovieInDetail(movieId: Int) {
        Log.d(TAG, "fetchMovieInDetail() called")
        Log.d(TAG, "movie Id: $movieId")

        /*// Build retrofit object
        val retrofit = Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

        val movieDbDetailsAsync: MovieDbService = retrofit.create(MovieDbService::class.java)

        CoroutineScope(Dispatchers.IO).launch {

            val request = movieDbDetailsAsync.getMovieDetailsAsync(movieId, Constants.API_KEY)
            withContext(Dispatchers.Main) {
                try {
                    val response = request.await()
                    if (response.isSuccessful) {
                        //Do something with response e.g show to the UI.
                        movieDetails.value = response.body()
                    } else {
                        Log.e(TAG, "Error: ${response.code()}")
                    }
                } catch (e: HttpException) {
                    Log.e(TAG, "Exception ${e.message}")
                } catch (e: Throwable) {
                    Log.e(TAG, "Oops: Something else went wrong ${e.message}")
                }
            }
        }*/

        val movieDbDetailsApi: MovieDbService = RetrofitFactory.create()
        val movieDetailsObservable = movieDbDetailsApi.fetchMovieDetails(movieId, Constants.API_KEY)

        movieDetailsObservable.enqueue(object : Callback<LiveData<Movie>> {
            override fun onResponse(call: Call<LiveData<Movie>>, response: Response<LiveData<Movie>>) {
                when {
                    response.isSuccessful -> {
                        Log.e(TAG, "response.isSuccessful: ${response.isSuccessful}")
                        movieDetails.postValue(response.body()?.value)
                    }
                    else -> {
                        Log.e(TAG, "Error: ${response.code()}")
                    }
                }
            }

            override fun onFailure(call: Call<LiveData<Movie>>, t: Throwable) {
                Log.e(TAG, t.localizedMessage)
                movieDetails.postValue(null)
            }
        })
    }
}

