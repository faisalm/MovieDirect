package com.moviedirect.detail

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.getColor
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.analytics.FirebaseAnalytics
import com.moviedirect.R
import com.moviedirect.common.Constants
import com.moviedirect.common.ViewModelFactory
import com.moviedirect.detail.model.Movie
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_movie_detail.*
import kotlinx.android.synthetic.main.movie_detail_content.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class MovieDetailFragment : Fragment(), View.OnClickListener {

    private val TAG = "MovieDetailFragment"

    private lateinit var viewModel: MovieDetailViewModel
    private lateinit var viewModelFactory: ViewModelFactory

    private var movieId: Int = 0
    private lateinit var mMovieUrl: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate() called with: savedInstanceState = [$savedInstanceState]")

        movieId = arguments!!.getInt(Constants.MOVIE_ID)
        Log.d(TAG, "movie Id: $movieId")

        // Update screen to Firebase - Manually track screens
        FirebaseAnalytics.getInstance(context!!).setCurrentScreen(activity!!, TAG, "movie_details")

        // View Model
        viewModelFactory = ViewModelFactory()
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MovieDetailViewModel::class.java)
        viewModel.setMovieId(movieId)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_movie_detail, container, false)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        toolbar.apply {
            inflateMenu(R.menu.menu_detail)

            try {
                (activity as AppCompatActivity).setSupportActionBar(toolbar)
                (activity as AppCompatActivity).supportActionBar?.setHomeButtonEnabled(true)
                (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
                (activity as AppCompatActivity).supportActionBar?.setShowHideAnimationEnabled(true)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        collapsing_toolbar.apply {
            setExpandedTitleColor(getColor(context, android.R.color.white))
            setCollapsedTitleTextColor(getColor(context, android.R.color.white))
            setCollapsedTitleTextColor(getColor(context, android.R.color.white))
        }

        imgWeb.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()

        // Listen to data change
        viewModel.getMovieDetail()?.observe(this, movieDetailObserver)
    }

    private var movieDetailObserver: Observer<Movie> = Observer { movieDetail ->
        if (movieDetail != null) {
            Log.d(TAG, "movieDetailObserver $movieDetail")
            bindMovieDetailsWithUi(movieDetail)
        }
    }

    override fun onPause() {
        viewModel.getMovieDetail()?.removeObserver(movieDetailObserver)

        super.onPause()
    }

    /**
     * @param movie object received from network.
     */
    private fun bindMovieDetailsWithUi(movie: Movie) {
        Log.w(TAG, "bindMovieDetailsWithUi $movie")

        // poster
        setMoviePoster(movie)

        // title, desc
        setMovieText(movie)

        // web page
        setMovieUrl(movie)

        // trailer
        setMovieTrailer(movie)

        // rel date
        setReleaseDate(movie)

        // genre
        setMovieGenre(movie)

        // others
        Log.d(TAG, "Locale: ${movie.original_language}")
        Log.d(TAG, "Status: ${movie.status}")
        Log.d(TAG, "Popularity: ${movie.popularity}")
        Log.d(TAG, "Languages?: ${movie.spoken_languages!!.size}")
        Log.d(TAG, "Languages: ${movie.spoken_languages.joinToString()}")
        Log.d(TAG, "VoteAverage: ${movie.vote_average} (${movie.vote_count})")
        Log.d(TAG, "=================")
    }

    /**
     *
     */
    private fun setMoviePoster(movie: Movie) {
        movie.backdrop_path.let { poster ->
            Log.d(TAG, "Poster path: $Constants.BASE_IMAGE_URL $poster")
            Picasso.get().load(Constants.BASE_IMAGE_URL + poster)
                    .fit().centerInside()
                    .placeholder(R.drawable.ic_movie_place_holder)
                    .error(R.drawable.ic_no_movie_place_holder)
                    .into(imgPoster)
        }
    }

    /**
     *
     */
    private fun setMovieText(movie: Movie) {
        Log.d(TAG, "Title: " + movie.original_title)
        collapsing_toolbar.title = movie.original_title

        Log.d(TAG, "Overview: " + movie.overview)
        tvContent.text = movie.overview
    }

    /**
     *
     */
    private fun setMovieUrl(movie: Movie) {
        movie.homepage.let { homePage ->
            imgWeb.visibility = View.VISIBLE
            mMovieUrl = homePage.toString()
            Log.d(TAG, "WebPage: $mMovieUrl")
        }
    }

    /**
     *
     */
    private fun setMovieTrailer(movie: Movie) {
        movie.video.let { video ->
            Log.d(TAG, "hasVideo: $video")
//            mPlayTrailerButton.setVisibility(View.VISIBLE);
        }
    }

    /**
     *
     */
    private fun setReleaseDate(movie: Movie) {
        movie.release_date.let { releaseDate ->
            Log.d(TAG, "Release: $releaseDate")

            try {
                val sdf = SimpleDateFormat("yyyy-mm-dd", Locale.getDefault())
                val parse = sdf.parse(releaseDate)
                val c = Calendar.getInstance()
                c.time = parse
                val year = c.get(Calendar.YEAR).toString()
                Log.d(TAG, "Year: $year")
                tvReleaseDate.text = year
            } catch (e: ParseException) {
                e.printStackTrace()
            }

        }
    }

    /**
     *
     */
    private fun setMovieGenre(movie: Movie) {
        movie.genres?.let { genres ->
            Log.d(TAG, "Genres : ${genres.joinToString()}")
            tvGenre.text = genres.joinToString()
        }
    }

    /**
     * @param url
     */
    private fun goToUrl(url: String?) {
        val uri = Uri.parse(url)
        val intent = Intent(Intent.ACTION_VIEW, uri)
        startActivity(intent)
    }

    override fun onClick(view: View) {
        Log.d(TAG, "onClick() called with: view = [" + view.id + "]")
        when (view.id) {
            /*case R.id.imgPlayer: break;*/
            R.id.imgWeb -> goToUrl(mMovieUrl)
            else -> {
            }
        }
    }
}
