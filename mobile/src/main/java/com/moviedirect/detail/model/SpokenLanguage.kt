package com.moviedirect.detail.model

/**
 * Model class represents the language data in which the [Movie] is avail.
 */
data class SpokenLanguage(
        var iso_639_1: String? = null,
        var name: String? = null
)