package com.moviedirect.detail.model

/**
 * Model class represents the [Movie] produced country.
 */
data class ProductionCountry(
        var iso_3166_1: String? = null,
        var name: String? = null
)