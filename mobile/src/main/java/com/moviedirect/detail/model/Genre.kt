package com.moviedirect.detail.model

/**
 * Model class represents the [Movie] genres.
 */
data class Genre(
        val id: Int = 0,
        val name: String? = null
)