package com.moviedirect.detail.model

/**
 * Model class represents the [Movie] production house.
 */
data class ProductionCompany(
        var id: Int = 0,
        var logo_path: String? = null,
        var name: String? = null,
        var origin_country: String? = null
)